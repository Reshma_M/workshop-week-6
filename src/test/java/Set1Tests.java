package org.bitbucket.zune;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.Ignore;
import org.junit.Rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple unit tests for Zune
 */
public class Set1Tests {

  /**
   *  Logger for this test suite
   */
  Logger logger = LoggerFactory.getLogger(Set1Tests.class);

  private Zune z = new Zune("franck");

  /**
   * First test
   */
  @Test
  public void test1()  {
    assertEquals(1980, z.computeYear( 330 ) );
  }


}
